PHPSKR (sms_phpskr)

Allows the SMS Framework to use phps.kr as a gateway.
Currently only basic sms sending functionality is implemented.
Rules integration is available thanks to SMS framework module.

Requirement:
Account of korean sms gateway service provider, phps.kr
http://www.phps.kr/smshosting_why.html

Dependencies:
SMS Framework

Installation:
Install as usual. for further information,
see https://drupal.org/node/895232

Configuration:
1. admin/smsframework/gateways.
Set phpskr as a default gateway.

2. admin/smsframework/gateways/phpskr
Fill out account, auth key, sending phone number.
If successfully connected with api, you'll see your phpskr balance.

Note:
Using sms_devel module, you can test to send actual sms message to
korean mobile phone. With rules & rules UI module, you can use
'send sms' action, for example when a payment is received,
or a new node is created, etc.
